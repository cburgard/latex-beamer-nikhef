## NIKHEF beamer theme

This is a beamer theme for NIKHEF. Use it with

You can move it to your `texmf` path (typically `/texmf/tex/latex/beamer/`) and run

~~~~~
texhash
~~~~~

to have LaTeX discover the new file. Alternatively, you can just copy the contents to the folder of your presentation. Then you can use it with

~~~~~
\usetheme{NIKHEF}
~~~~~

Enjoy!